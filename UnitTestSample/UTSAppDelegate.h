//
//  UTSAppDelegate.h
//  UnitTestSample
//
//  Created by Gökhan Barış Aker on 26/05/14.
//  Copyright (c) 2014 Mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UTSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
