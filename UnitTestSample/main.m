//
//  main.m
//  UnitTestSample
//
//  Created by Gökhan Barış Aker on 26/05/14.
//  Copyright (c) 2014 Mobilike. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UTSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UTSAppDelegate class]));
    }
}
