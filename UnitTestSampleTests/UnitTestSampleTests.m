//
//  UnitTestSampleTests.m
//  UnitTestSampleTests
//
//  Created by Gökhan Barış Aker on 26/05/14.
//  Copyright (c) 2014 Mobilike. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface UnitTestSampleTests : XCTestCase

@end

@implementation UnitTestSampleTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSuccess
{
    XCTAssertTrue(YES, @"Much test, so refreshing...");
}

@end
